import React from 'react'
import { faHome, faCoffee, faRadio } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const FontAwesome = () => {
    return (
        <div>
            <FontAwesomeIcon icon={faHome} />
            <FontAwesomeIcon icon={faCoffee} />
            <FontAwesomeIcon icon={faRadio} />
        </div>
    )
}

export default FontAwesome