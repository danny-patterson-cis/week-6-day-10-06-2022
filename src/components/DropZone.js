import React from "react";
import { useDropzone } from "react-dropzone";

const DropZone = () => {
    const { acceptedFiles, getRootProps, getInputProps } = useDropzone();

    const files = acceptedFiles.map((file) => (
        <li key={file.path}>
            {file.path} - {file.size} bytes
        </li>
    ));

    return (
        <div>
            <section className="p-4 w-100 border container">
                <div {...getRootProps({ className: "dropzone" })}>
                    <input {...getInputProps()} />
                    <h1>Drag 'n' drop some files here, or click to select files</h1>
                </div>
                <aside>
                    <h4>Files</h4>
                    <ul>{files}</ul>
                </aside>
            </section>
        </div>
    )
}

export default DropZone