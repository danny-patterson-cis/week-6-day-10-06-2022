import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const ReactTostify = () => {
    const notify = () => {
        toast('Basic notification', { position: toast.POSITION.TOP_LEFT, autoClose: 8000 })
        toast('Basic notification', { position: toast.POSITION.TOP_CENTER })
        toast('Basic notification', { position: toast.POSITION.TOP_RIGHT })
        toast('Basic notification', { position: toast.POSITION.BOTTOM_LEFT })
        toast('Basic notification', { position: toast.POSITION.BOTTOM_RIGHT })
        toast('Basic notification', { position: toast.POSITION.BOTTOM_CENTER, autoClose: 8000 })

    }
    return (
        <div>
            <button onClick={notify}>Notify!</button>
            <ToastContainer />
        </div>
    )
}

export default ReactTostify