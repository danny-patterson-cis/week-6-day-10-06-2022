import React from 'react'
import Select from 'react-select';
const ReactSelect = () => {
    const options = [
        { value: 'blues', label: 'Blues' },
        { value: 'rock', label: 'Rock' },
        { value: 'jazz', label: 'Jazz' },
        { value: 'orchestra', label: 'Orchestra' }
    ];
    return (
        <div>
            <Select options={options} />
        </div>
    )
}

export default ReactSelect