import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    DropdownMenu, DropdownItem,
    ButtonDropdown, DropdownToggle
} from "reactstrap"

const Reactstrap = () => {
    const [dropdownOpen, setOpen] = React.useState(false);
    return (
        <div><div style={{
            display: 'block', width: 700, padding: 30
        }}>
            <h4>ReactJS Reactstrap ButtonDropdown Component</h4>
            <ButtonDropdown toggle={() => { setOpen(!dropdownOpen) }}
                isOpen={dropdownOpen}>
                <DropdownToggle className="bg-primary" caret>
                    Sample Button Dropdown
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem header>Numeric Characters
                    </DropdownItem>
                    <DropdownItem>One</DropdownItem>
                    <DropdownItem>Two</DropdownItem>
                    <DropdownItem>Three</DropdownItem>
                    <DropdownItem>Four</DropdownItem>
                    <DropdownItem>Five</DropdownItem>
                    <DropdownItem>Six</DropdownItem>
                    <DropdownItem>Seven</DropdownItem>
                    <DropdownItem>Eight</DropdownItem>
                    <DropdownItem>Nine</DropdownItem>
                    <DropdownItem>Zero</DropdownItem>
                </DropdownMenu>
            </ButtonDropdown>
        </div >
        </div>
    )
}

export default Reactstrap