import React from 'react'
import classnames from "classnames";
import '../App.css'

const Message = ({ status, error }) => (
    <div
        className={classnames("message", {
            active: status === "active",
            pending: status === "pending",
            whoops: error === false
        })}
    >
        You got mail!
    </div>
);

const Classname = () => {
    return (
        <div> <Message status="active" error={false} /></div>
    )
}

export default Classname;