import React, { Component } from 'react'
import LaddaButton, { XL, SLIDE_UP } from '@zumper/react-ladda'

class MyButton extends Component {
    state = { loading: false, progress: 0 }

    onClick = () => {
        this.setState((state) => ({
            loading: true,
            progress: 0.5,
        }))
    }

    render() {
        const { loading, progress } = this.state
        return (
            <LaddaButton
                loading={loading}
                progress={progress}
                onClick={this.onClick}
                color="mint"
                size={XL}
                style={SLIDE_UP}
                spinnerSize={30}
                spinnerColor="#ddd"
                spinnerLines={12}
            >
                Click Here!
            </LaddaButton>
        )
    }
}

export default MyButton