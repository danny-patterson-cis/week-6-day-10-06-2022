import React from 'react'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import img1 from './img/1007550.jpg'
import img2 from './img/1034735.png'
import img3 from './img/1043544.png'
import img4 from './img/1061065.png'
import img5 from './img/1064341.png'

const ReactMutlticarsouel = () => {
    return (
        <div className="mx-auto w-50 mt-3 p-5">
            <h3> Demo For react-multi-carousel </h3>
            <Carousel
                additionalTransfrom={0}
                arrows
                autoPlaySpeed={2000}
                centerMode={false}
                className=""
                containerClass="container"
                dotListClass=""
                draggable
                focusOnSelect={false}
                infinite
                itemClass=""
                keyBoardControl
                minimumTouchDrag={80}
                renderButtonGroupOutside={false}
                renderDotsOutside
                responsive={{
                    desktop: {
                        breakpoint: {
                            max: 3000,
                            min: 1024,
                        },
                        items: 1,
                    },
                    mobile: {
                        breakpoint: {
                            max: 464,
                            min: 0,
                        },
                        items: 1,
                    },
                    tablet: {
                        breakpoint: {
                            max: 1024,
                            min: 464,
                        },
                        items: 1,
                    },
                }}
                showDots
                sliderClass=""
                slidesToSlide={1}
                swipeable
            >
                <img
                    src={img1}
                    alt=""
                    style={{
                        display: "block",
                        height: "100%",
                        margin: "auto",
                        width: "100%",
                    }}
                />
                <img
                    src={img2}
                    alt=""
                    style={{
                        display: "block",
                        height: "100%",
                        margin: "auto",
                        width: "100%",
                    }}
                />
                <img
                    src={img3}
                    alt=""
                    style={{
                        display: "block",
                        height: "100%",
                        margin: "auto",
                        width: "100%",
                    }}
                />
                <img
                    src={img4}
                    alt=""
                    style={{
                        display: "block",
                        height: "100%",
                        margin: "auto",
                        width: "100%",
                    }}
                />
                <img
                    src={img5}
                    alt=""
                    style={{
                        display: "block",
                        height: "100%",
                        margin: "auto",
                        width: "100%",
                    }}
                />
            </Carousel>
        </div>
    )
}

export default ReactMutlticarsouel