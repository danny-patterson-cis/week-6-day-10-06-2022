import React from 'react'
import { useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Classname from './components/Classname';
import DatePickerDemo from './components/DatePicker';
import DetectOutside from './components/DetectOutside';
import DropZone from './components/DropZone';
import FontAwesome from './components/FontAwesome';
import RcSlider from './components/RcSlider';
import ReactMutlticarsouel from './components/ReactMutlticarsouel';
import { PaginationDemo } from './components/ReactPagination';
import ReactSelect from './components/ReactSelect';
import Reactstrap from './components/Reactstrap';
import ReactTostify from './components/ReactTostify';
import Table6 from './components/Table-6';
import MyButton from './components/ZummperLadda';

const Nav = () => {
    let [showInfo1, setShowInfo1] = useState(false);
    return (
        <Router>
            <div>
                <nav className="navbar navbar-expand-lg bg-light ">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex justify-content-between">
                                <li className="nav-item me-2">
                                    <Link to="/">className</Link>
                                </li>
                                <li className="nav-item me-2 ">
                                    <Link to="/DatePicker">DatePicker</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/DetectOutside">DetectOutside</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/DropZone">DropZone</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/FontAwesome">FontAwesome</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/RcSilder">RcSilder</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/ReactMulticarsouel">ReactMutlticarsouel</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/ReactPagination">ReactPagination</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/ReactSelect">ReactSelect</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/Reactstrap">Reactstrap</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/ReactTostify">ReactTostify</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/Table6">Table</Link>
                                </li>
                                <li className="nav-item me-2">
                                    <Link to="/ZumperLadda">ZumperLadda</Link>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
                <Switch>
                    <Route path="/ZumperLadda">
                        <MyButton />
                    </Route>
                    <Route path="/Table6">
                        <Table6 />
                    </Route>
                    <Route path="/ReactTostify">
                        <ReactTostify />
                    </Route>
                    <Route path="/Reactstrap">
                        <Reactstrap />
                    </Route>
                    <Route path="/ReactSelect">
                        <ReactSelect />
                    </Route>
                    <Route path="/ReactPagination">
                        <PaginationDemo />
                    </Route>
                    <Route path="/ReactMulticarsouel">
                        <ReactMutlticarsouel />
                    </Route>
                    <Route path="/RcSilder">
                        <RcSlider />
                    </Route>
                    <Route path="/FontAwesome">
                        <FontAwesome />
                    </Route>
                    <Route path="/DropZone">
                        <DropZone />
                    </Route>
                    <Route path="/DetectOutside">
                        <div className="container">
                            <div className="info-box-wrapper">
                                <button onClick={() => { setShowInfo1(true) }} style={{ marginRight: '4px' }}>Show Detect outside Functional</button>
                                <DetectOutside show={showInfo1} onClickOutside={() => { setShowInfo1(false) }} message="Click outside to close this" />
                            </div>
                        </div>
                    </Route>
                    <Route path="/DatePicker">
                        <DatePickerDemo />
                    </Route>
                    <Route path="/">
                        <Classname />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

export default Nav